import { Component, OnInit } from '@angular/core';
//import { Router } from '@angular/Router';
import { ServiceService } from '../../Service/service.service';
import { Producto } from 'src/app/Modelo/Producto';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

  productos:Producto[];

  constructor( private service:ServiceService ) { }

  ngOnInit(): void {

    console.log("Entra listar");
    this.service.getProducts().subscribe(data=>{
      this.productos=data;
    })
  }

}
