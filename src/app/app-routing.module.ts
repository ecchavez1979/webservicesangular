import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarComponent } from './Producto/listar/listar.component';
import { NuevoComponent } from './Producto/nuevo/nuevo.component';

const routes: Routes = [
{path:'listar', component:ListarComponent},
{path:'nuevo', component:NuevoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
